/// @description Insert description here
// You can write your code in this editor
for(var i = 0; i < array_length_1d(GM.Characters); i++)
{
	with(instance_create_depth(room_width / 2, room_height / 2 + 72 * i, 0, Button))
	{
		Character = GM.Characters[i];
		Text = Character.Name;
		ClickScript = SelectCharacter;
	}
}