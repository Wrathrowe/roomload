{
    "id": "0723a0bb-d25d-46ef-a4f9-8e775e83f5da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GameMaster",
    "eventList": [
        {
            "id": "33da9952-1fe1-456b-aab8-d5faa951fefe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0723a0bb-d25d-46ef-a4f9-8e775e83f5da"
        },
        {
            "id": "9a3e0110-555f-42af-bde4-06fff0d2b6f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "0723a0bb-d25d-46ef-a4f9-8e775e83f5da"
        },
        {
            "id": "d08cec55-301e-4449-bf40-0c04b25ddf32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "0723a0bb-d25d-46ef-a4f9-8e775e83f5da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}