/// @description Insert description here
// You can write your code in this editor

var filename = "savegame.txt";

if(!file_exists(filename))
{
	CreatePlayers(filename);	
}

Characters = LoadPlayers(filename);

for(var i = 0; i < array_length_1d(Characters); i++)
{
	Characters[i].visible = false;
}

SelectedCharacter = Characters[0];