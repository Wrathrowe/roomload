{
    "id": "640f5dc9-561a-4e18-8e0b-78c22e4a9c5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c800e027-70aa-4d2d-a248-7517d3864cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640f5dc9-561a-4e18-8e0b-78c22e4a9c5a",
            "compositeImage": {
                "id": "e7918146-ca82-4910-9826-f314d70942d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c800e027-70aa-4d2d-a248-7517d3864cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9f7e51e-ff03-4de4-8dea-44f986bb1a46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c800e027-70aa-4d2d-a248-7517d3864cea",
                    "LayerId": "380f3c21-7f42-492a-99af-070c1de06dd6"
                }
            ]
        },
        {
            "id": "2480cc58-adb1-4ae9-abb5-cd04f7c8cb3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640f5dc9-561a-4e18-8e0b-78c22e4a9c5a",
            "compositeImage": {
                "id": "aee786be-6a71-4b0f-a07c-ad61b5309c52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2480cc58-adb1-4ae9-abb5-cd04f7c8cb3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a20dcb-ed90-4a6e-9398-2ef669ad5398",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2480cc58-adb1-4ae9-abb5-cd04f7c8cb3d",
                    "LayerId": "380f3c21-7f42-492a-99af-070c1de06dd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "380f3c21-7f42-492a-99af-070c1de06dd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "640f5dc9-561a-4e18-8e0b-78c22e4a9c5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}