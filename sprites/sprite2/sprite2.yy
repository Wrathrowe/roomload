{
    "id": "659654d4-56ee-4f04-9781-2076182a320b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "639447ef-027a-4ca9-93af-85dbd9970041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "659654d4-56ee-4f04-9781-2076182a320b",
            "compositeImage": {
                "id": "078072ba-04df-487b-9572-e814ae422b21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "639447ef-027a-4ca9-93af-85dbd9970041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd53d17f-9d54-4fe8-bc48-2d30c968246e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "639447ef-027a-4ca9-93af-85dbd9970041",
                    "LayerId": "431617ca-4f2f-4009-8604-53f36f03373c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "431617ca-4f2f-4009-8604-53f36f03373c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "659654d4-56ee-4f04-9781-2076182a320b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}