{
    "id": "20fa797a-9c3b-4bfa-912c-22482a75355a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d772aa4a-cf8d-4c7b-90b5-c97d4b5e6b63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20fa797a-9c3b-4bfa-912c-22482a75355a",
            "compositeImage": {
                "id": "13a97041-516a-4b9e-9c96-50c8b37ed52d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d772aa4a-cf8d-4c7b-90b5-c97d4b5e6b63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31481bc3-7c3d-417d-9ff4-94093abb4b87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d772aa4a-cf8d-4c7b-90b5-c97d4b5e6b63",
                    "LayerId": "faaf69e6-638c-4614-a06e-943eb5f3d60d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "faaf69e6-638c-4614-a06e-943eb5f3d60d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20fa797a-9c3b-4bfa-912c-22482a75355a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}