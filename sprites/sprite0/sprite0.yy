{
    "id": "d49e6533-90d1-48c5-972f-486e9831070d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "306ecb73-9206-4eb3-b415-9afa6e6ad9ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d49e6533-90d1-48c5-972f-486e9831070d",
            "compositeImage": {
                "id": "ce97ecae-d809-4d22-b63d-6bc25ac8ed1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "306ecb73-9206-4eb3-b415-9afa6e6ad9ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5159cc3b-5b7f-4140-aef4-3462530188a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "306ecb73-9206-4eb3-b415-9afa6e6ad9ab",
                    "LayerId": "80c91f0c-81e1-4bdd-86df-630ba7750c08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "80c91f0c-81e1-4bdd-86df-630ba7750c08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d49e6533-90d1-48c5-972f-486e9831070d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}