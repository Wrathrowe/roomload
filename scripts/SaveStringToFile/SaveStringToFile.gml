var filename = argument0;
var s = argument1;

var b = buffer_create(string_byte_length(s) + 1, buffer_fixed, 1);
buffer_write(b, buffer_string, s);
buffer_save(b, filename);
buffer_delete(b);