var filename = argument0;

var wrapper = LoadJSONFromFile(filename);
var l = wrapper[? "ROOT"];

var characters;
	
for(var i = 0; i < ds_list_size(l); i++)
{
	var m = l[| i];
		
	var player = instance_create_layer(0, 0, "Instances", PlayerBase);
		
	with(player)
	{
		Name = m[? "Name"];
		Team = m[? "Team"];
		sprite_index = m[? "sprite_index"];
	}
		
	characters[i] = player;
}
	
ds_map_destroy(wrapper);
	
return characters;