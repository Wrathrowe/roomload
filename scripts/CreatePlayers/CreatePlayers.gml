var filename = argument0;

var root_list = ds_list_create();
var map = ds_map_create();
ds_list_add(root_list, map);
	
// save the data, not the pointer
ds_list_mark_as_map(root_list, ds_list_size(root_list) - 1);

ds_map_add(map, "Name", "Bob");
ds_map_add(map, "Team", "A");
ds_map_add(map, "sprite_index", "0");

map = ds_map_create();
ds_list_add(root_list, map);
	
// save the data, not the pointer
ds_list_mark_as_map(root_list, ds_list_size(root_list) - 1);

ds_map_add(map, "Name", "Jane");
ds_map_add(map, "Team", "B");
ds_map_add(map, "sprite_index", "1");

// wrap the root list in a map, JSON likes it better
var wrapper = ds_map_create();
ds_map_add_list(wrapper, "ROOT", root_list);

// save all this to a string
var s = json_encode(wrapper);
SaveStringToFile(filename, s);