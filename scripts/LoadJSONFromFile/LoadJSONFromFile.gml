var filename = argument0;

var b = buffer_load(filename);
var s = buffer_read(b, buffer_string);
buffer_delete(b);

var json = json_decode(s);
return json;